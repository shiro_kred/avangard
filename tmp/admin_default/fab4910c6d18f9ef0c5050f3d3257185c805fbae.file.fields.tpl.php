<?php /* Smarty version Smarty-3.1.19, created on 2016-05-17 05:04:06
         compiled from "/Users/dmitry/Desktop/авангард-клининг.рф/admin/templates/default/fields.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1182913725573ade8690a5a0-17740584%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fab4910c6d18f9ef0c5050f3d3257185c805fbae' => 
    array (
      0 => '/Users/dmitry/Desktop/авангард-клининг.рф/admin/templates/default/fields.tpl',
      1 => 1455472436,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1182913725573ade8690a5a0-17740584',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'pageAction' => 0,
    'item' => 0,
    'items' => 0,
    'entry' => 0,
    'fieldTypes' => 0,
    'type' => 0,
    'groups' => 0,
    'code' => 0,
    'value' => 0,
    'core' => 0,
    'language' => 0,
    'tooltips' => 0,
    'pages' => 0,
    'pageId' => 0,
    'parents' => 0,
    'field_item' => 0,
    'item_list' => 0,
    'field_name' => 0,
    'elements' => 0,
    'element' => 0,
    'key' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_573ade874c09f4_08630212',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573ade874c09f4_08630212')) {function content_573ade874c09f4_08630212($_smarty_tpl) {?><?php if (!is_callable('smarty_function_html_radio_switcher')) include '/Users/dmitry/Desktop/авангард-клининг.рф/includes/smarty/intelli_plugins/function.html_radio_switcher.php';
if (!is_callable('smarty_function_ia_hooker')) include '/Users/dmitry/Desktop/авангард-клининг.рф/includes/smarty/intelli_plugins/function.ia_hooker.php';
?><form method="post" class="sap-form form-horizontal">
	<?php echo iaSmarty::preventCsrf(array(),$_smarty_tpl);?>


	<div class="wrap-list">
		<div class="wrap-group">
			<div class="wrap-group-heading">
				<h4><?php echo iaSmarty::lang(array('key'=>'options'),$_smarty_tpl);?>
</h4>
			</div>

			<?php if (iaCore::ACTION_EDIT==$_smarty_tpl->tpl_vars['pageAction']->value) {?>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'name'),$_smarty_tpl);?>
</label>

				<div class="col col-lg-4">
					<input type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" disabled>
					<input type="hidden" name="name" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
">
				</div>
			</div>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_item'),$_smarty_tpl);?>
</label>

				<div class="col col-lg-4">
					<input type="text" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['item'], ENT_QUOTES, 'UTF-8', true);?>
" disabled>
					<input type="hidden" name="item" id="input-item" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['item'], ENT_QUOTES, 'UTF-8', true);?>
">
				</div>
			</div>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_type'),$_smarty_tpl);?>
</label>

				<div class="col col-lg-4">
					<input type="text" value="<?php echo iaSmarty::lang(array('key'=>"field_type_".((string)$_smarty_tpl->tpl_vars['item']->value['type']),'default'=>$_smarty_tpl->tpl_vars['item']->value['type']),$_smarty_tpl);?>
" disabled>
					<input type="hidden" name="type" id="input-type" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['type'], ENT_QUOTES, 'UTF-8', true);?>
">
				</div>
			</div>
			<?php } else { ?>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'name'),$_smarty_tpl);?>
 <span class="required">*</span></label>

				<div class="col col-lg-4">
					<input type="text" name="name" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
">
					<p class="help-block"><?php echo iaSmarty::lang(array('key'=>'unique_name'),$_smarty_tpl);?>
</p>
				</div>
			</div>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_item'),$_smarty_tpl);?>
 <span class="required">*</span></label>

				<div class="col col-lg-4">
					<select name="item" id="input-item">
						<option value=""><?php echo iaSmarty::lang(array('key'=>'_select_'),$_smarty_tpl);?>
</option>
						<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value) {
$_smarty_tpl->tpl_vars['entry']->_loop = true;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['entry']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['item']->value['item']==$_smarty_tpl->tpl_vars['entry']->value) {?> selected<?php }?>><?php echo iaSmarty::lang(array('key'=>$_smarty_tpl->tpl_vars['entry']->value,'default'=>$_smarty_tpl->tpl_vars['entry']->value),$_smarty_tpl);?>
</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_type'),$_smarty_tpl);?>
 <span class="required">*</span></label>

				<div class="col col-lg-4">
					<select name="type" id="input-type">
						<option value=""><?php echo iaSmarty::lang(array('key'=>'_select_'),$_smarty_tpl);?>
</option>
						<?php  $_smarty_tpl->tpl_vars['type'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['type']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['fieldTypes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['type']->key => $_smarty_tpl->tpl_vars['type']->value) {
$_smarty_tpl->tpl_vars['type']->_loop = true;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['type']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['item']->value['type']==$_smarty_tpl->tpl_vars['type']->value) {?> selected<?php }?> data-annotation="<?php echo iaSmarty::lang(array('key'=>"field_type_tip_".((string)$_smarty_tpl->tpl_vars['type']->value),'default'=>''),$_smarty_tpl);?>
"><?php echo iaSmarty::lang(array('key'=>"field_type_".((string)$_smarty_tpl->tpl_vars['type']->value),'default'=>$_smarty_tpl->tpl_vars['type']->value),$_smarty_tpl);?>
</option>
						<?php } ?>
					</select>
					<p class="help-block"></p>
				</div>
			</div>
			<?php }?>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_group'),$_smarty_tpl);?>
</label>

				<div class="col col-lg-4">
					<select name="fieldgroup_id" id="input-fieldgroup"<?php if (!$_smarty_tpl->tpl_vars['groups']->value) {?> disabled<?php }?>>
						<option value=""><?php echo iaSmarty::lang(array('key'=>'_select_'),$_smarty_tpl);?>
</option>
						<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['groups']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['code']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['code']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['code']->value==$_smarty_tpl->tpl_vars['item']->value['fieldgroup_id']) {?> selected<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
</option>
						<?php } ?>
					</select>
				</div>
			</div>
			<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
 $_smarty_tpl->tpl_vars['code']->value = $_smarty_tpl->tpl_vars['language']->key;
?>
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'title'),$_smarty_tpl);?>
 <span class="required">*</span> <span class="label label-info"><?php echo $_smarty_tpl->tpl_vars['language']->value['title'];?>
</span></label>

					<div class="col col-lg-4">
						<input type="text" name="title[<?php echo $_smarty_tpl->tpl_vars['code']->value;?>
]"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['title'][$_smarty_tpl->tpl_vars['code']->value])) {?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['title'][$_smarty_tpl->tpl_vars['code']->value], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
					</div>
				</div>
			<?php } ?>
			<div id="js-row-empty-text" class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'empty_field'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['empty_field'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-4">
					<input type="text" name="empty_field" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['empty_field'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['empty_field'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
				</div>
			</div>
			<?php if ($_smarty_tpl->tpl_vars['pages']->value) {?>
				<div class="row" id="js-pages-list-row"<?php if (iaCore::ACTION_ADD==$_smarty_tpl->tpl_vars['pageAction']->value&&(!$_POST&&!isset($_GET['item']))) {?> style="display: none;"<?php }?>>
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'shown_on_pages'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<div class="box-simple fieldset">
						<?php  $_smarty_tpl->tpl_vars['entry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['entry']->_loop = false;
 $_smarty_tpl->tpl_vars['pageId'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['pages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['entry']->key => $_smarty_tpl->tpl_vars['entry']->value) {
$_smarty_tpl->tpl_vars['entry']->_loop = true;
 $_smarty_tpl->tpl_vars['pageId']->value = $_smarty_tpl->tpl_vars['entry']->key;
?>
							<div class="checkbox" data-item="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['entry']->value['item'], ENT_QUOTES, 'UTF-8', true);?>
"<?php if ($_smarty_tpl->tpl_vars['item']->value['item']!=$_smarty_tpl->tpl_vars['entry']->value['item']) {?> style="display: none;"<?php }?>>
								<label>
									<input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['entry']->value['name'];?>
"<?php if (in_array($_smarty_tpl->tpl_vars['entry']->value['name'],$_smarty_tpl->tpl_vars['item']->value['pages'])) {?> checked<?php }?> name="pages[<?php echo $_smarty_tpl->tpl_vars['pageId']->value;?>
]">
									<?php echo $_smarty_tpl->tpl_vars['entry']->value['title'];?>

								</label>
							</div>
						<?php } ?>
						</div>
						<a href="#" id="toggle-pages" class="label label-default pull-right"><i class="i-lightning"></i> <?php echo iaSmarty::lang(array('key'=>'select_all'),$_smarty_tpl);?>
</a>
					</div>
				</div>
			<?php }?>
			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'visible_for_admin'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['adminonly'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-4">
					<?php echo smarty_function_html_radio_switcher(array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['adminonly'])===null||$tmp==='' ? 0 : $tmp),'name'=>'adminonly'),$_smarty_tpl);?>

				</div>
			</div>

			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'regular_field'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['regular_field'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-4">
					<select name="relation" class="common" id="js-field-relation">
						<option value="regular"<?php if (iaField::RELATION_REGULAR==$_smarty_tpl->tpl_vars['item']->value['relation']) {?> selected<?php }?>><?php echo iaSmarty::lang(array('key'=>'field_relation_regular'),$_smarty_tpl);?>
</option>
						<option value="parent"<?php if (iaField::RELATION_PARENT==$_smarty_tpl->tpl_vars['item']->value['relation']) {?> selected<?php }?>><?php echo iaSmarty::lang(array('key'=>'field_relation_parent'),$_smarty_tpl);?>
</option>
						<option value="dependent"<?php if (iaField::RELATION_DEPENDENT==$_smarty_tpl->tpl_vars['item']->value['relation']) {?> selected<?php }?>><?php echo iaSmarty::lang(array('key'=>'field_relation_dependent'),$_smarty_tpl);?>
</option>
					</select>
				</div>
			</div>

			<div class="row" id="regular_field"<?php if (iaField::RELATION_DEPENDENT!=$_smarty_tpl->tpl_vars['item']->value['relation']) {?> style="display: none;"<?php }?>>
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'host_fields'),$_smarty_tpl);?>
</label>

				<div class="col col-lg-4">
					<?php  $_smarty_tpl->tpl_vars['item_list'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_list']->_loop = false;
 $_smarty_tpl->tpl_vars['field_item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['parents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_list']->key => $_smarty_tpl->tpl_vars['item_list']->value) {
$_smarty_tpl->tpl_vars['item_list']->_loop = true;
 $_smarty_tpl->tpl_vars['field_item']->value = $_smarty_tpl->tpl_vars['item_list']->key;
?>
						<div class="js-dependent-fields-list" data-item="<?php echo $_smarty_tpl->tpl_vars['field_item']->value;?>
"<?php if ($_smarty_tpl->tpl_vars['item']->value['item']!=$_smarty_tpl->tpl_vars['field_item']->value) {?> style="display: none;"<?php }?>>
							<div class="list-group list-group-accordion">
								<?php  $_smarty_tpl->tpl_vars['elements'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['elements']->_loop = false;
 $_smarty_tpl->tpl_vars['field_name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item_list']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['elements']->index=-1;
foreach ($_from as $_smarty_tpl->tpl_vars['elements']->key => $_smarty_tpl->tpl_vars['elements']->value) {
$_smarty_tpl->tpl_vars['elements']->_loop = true;
 $_smarty_tpl->tpl_vars['field_name']->value = $_smarty_tpl->tpl_vars['elements']->key;
 $_smarty_tpl->tpl_vars['elements']->index++;
 $_smarty_tpl->tpl_vars['elements']->first = $_smarty_tpl->tpl_vars['elements']->index === 0;
?>
									<a href="#" class="list-group-item<?php if ($_smarty_tpl->tpl_vars['elements']->first) {?> active<?php }?>">
										<p class="list-group-item-heading"><b><?php echo iaSmarty::lang(array('key'=>"field_".((string)$_smarty_tpl->tpl_vars['field_name']->value)),$_smarty_tpl);?>
 <?php echo iaSmarty::lang(array('key'=>'field_values'),$_smarty_tpl);?>
</b></p>
									</a>
									<div class="list-group-item fields-list"<?php if (!$_smarty_tpl->tpl_vars['elements']->first) {?> style="display:none;"<?php }?>>
										<?php  $_smarty_tpl->tpl_vars['element'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['element']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['elements']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['element']->key => $_smarty_tpl->tpl_vars['element']->value) {
$_smarty_tpl->tpl_vars['element']->_loop = true;
?>
											<div class="checkbox">
												<label>
													<input type="checkbox" value="1"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['parents'][$_smarty_tpl->tpl_vars['field_item']->value][$_smarty_tpl->tpl_vars['field_name']->value][$_smarty_tpl->tpl_vars['element']->value])) {?> checked<?php }?> name="parents[<?php echo $_smarty_tpl->tpl_vars['field_item']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['field_name']->value;?>
][<?php echo $_smarty_tpl->tpl_vars['element']->value;?>
]">
													<?php echo iaSmarty::lang(array('key'=>"field_".((string)$_smarty_tpl->tpl_vars['field_name']->value)."_".((string)$_smarty_tpl->tpl_vars['element']->value)),$_smarty_tpl);?>

												</label>
											</div>
										<?php } ?>
									</div>
								<?php }
if (!$_smarty_tpl->tpl_vars['elements']->_loop) {
?>
									<span class="list-group-item list-group-item-info"><?php echo iaSmarty::lang(array('key'=>'no_parent_fields'),$_smarty_tpl);?>
</span>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>

			<div class="row" id="for_plan_only" <?php if ($_smarty_tpl->tpl_vars['item']->value['required']) {?> style="display: none"<?php }?>>
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'for_plan_only'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['for_plan_only'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-4">
					<?php echo smarty_function_html_radio_switcher(array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['for_plan'])===null||$tmp==='' ? 0 : $tmp),'name'=>'for_plan'),$_smarty_tpl);?>

				</div>
			</div>

			<div class="row"<?php if ($_smarty_tpl->tpl_vars['item']->value['required']) {?> style="display: none"<?php }?>>
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'searchable'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['searchable'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-4">
					<?php echo smarty_function_html_radio_switcher(array('value'=>$_smarty_tpl->tpl_vars['item']->value['searchable'],'name'=>'searchable'),$_smarty_tpl);?>

				</div>
			</div>

			<div class="row" id="link-to-details" style="display: none">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'link_to'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['link_to_details'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-4">
					<?php echo smarty_function_html_radio_switcher(array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['link_to'])===null||$tmp==='' ? 0 : $tmp),'name'=>'link_to'),$_smarty_tpl);?>

				</div>
			</div>

			<?php echo smarty_function_ia_hooker(array('name'=>'smartyAdminFieldsEdit'),$_smarty_tpl);?>


			<div id="text" class="field_type" style="display: none;">
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_length'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<input class="js-filter-numeric" type="text" name="text_length" value="<?php if (!$_smarty_tpl->tpl_vars['item']->value['length']||$_smarty_tpl->tpl_vars['item']->value['length']>255) {?>255<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['item']->value['length'];?>
<?php }?>">
						<p class="help-block"><?php echo iaSmarty::lang(array('key'=>'digits_only'),$_smarty_tpl);?>
</p>
					</div>
				</div>
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_default'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<input type="text" name="text_default" class="js-code-editor" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['default'];?>
">
					</div>
				</div>
			</div>

			<div id="textarea" class="field_type" style="display: none;">
				<div id="js-row-use-editor" class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'use_editor'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<?php echo smarty_function_html_radio_switcher(array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['use_editor'])===null||$tmp==='' ? 1 : $tmp),'name'=>'use_editor'),$_smarty_tpl);?>

					</div>
				</div>
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_length'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<input type="text" name="length" class="js-code-editor" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['length'], ENT_QUOTES, 'UTF-8', true);?>
">
					</div>
				</div>
			</div>

			<div id="storage" class="field_type" style="display: none;">
				<?php if (!is_writable(@constant('IA_UPLOADS'))) {?>
				<div class="row">
					<label class="col col-lg-2 control-label"></label>

					<div class="col col-lg-4">
						<div class="alert alert-warning"><?php echo iaSmarty::lang(array('key'=>'upload_writable_permission'),$_smarty_tpl);?>
</div>
					</div>
				</div>
				<?php } else { ?>
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'max_files'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<input class="js-filter-numeric" type="text" name="max_files" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['length'], ENT_QUOTES, 'UTF-8', true);?>
">
					</div>
				</div>
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'file_types'),$_smarty_tpl);?>
 <span class="required">*</span></label>

					<div class="col col-lg-4">
						<textarea rows="3" id="file_types" name="file_types"><?php if (isset($_smarty_tpl->tpl_vars['item']->value['file_types'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['file_types'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
					</div>
				</div>
				<?php }?>
			</div>

			<div id="image" class="field_type" style="display: none;">
				<?php if (!is_writable(@constant('IA_UPLOADS'))) {?>
					<div class="row">
						<label class="col col-lg-2 control-label"></label>

						<div class="col col-lg-4">
							<div class="alert alert-warning"><?php echo iaSmarty::lang(array('key'=>'upload_writable_permission'),$_smarty_tpl);?>
</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'file_prefix'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<input type="text" name="file_prefix" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['file_prefix'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['file_prefix'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
						</div>
					</div>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'image_width'),$_smarty_tpl);?>
 / <?php echo iaSmarty::lang(array('key'=>'image_height'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<div class="row">
								<div class="col col-lg-6">
									<input type="text" name="image_width" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['image_width'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['image_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>900<?php }?>">
								</div>
								<div class="col col-lg-6">
									<input type="text" name="image_height" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['image_height'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['image_height'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>600<?php }?>">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'thumb_width'),$_smarty_tpl);?>
 / <?php echo iaSmarty::lang(array('key'=>'thumb_height'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<div class="row">
								<div class="col col-lg-6">
									<input type="text" name="thumb_width" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['thumb_width'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['thumb_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['core']->value['config']['thumb_w'];?>
<?php }?>">
								</div>
								<div class="col col-lg-6">
									<input type="text" name="thumb_height" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['thumb_height'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['thumb_height'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['core']->value['config']['thumb_h'];?>
<?php }?>">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'resize_mode'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<select name="resize_mode">
								<option value="crop"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['resize_mode'])&&iaPicture::CROP==$_smarty_tpl->tpl_vars['item']->value['resize_mode']) {?> selected<?php }?> data-annotation="<?php echo iaSmarty::lang(array('key'=>'crop_tip'),$_smarty_tpl);?>
"><?php echo iaSmarty::lang(array('key'=>'crop'),$_smarty_tpl);?>
</option>
								<option value="fit"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['resize_mode'])&&iaPicture::FIT==$_smarty_tpl->tpl_vars['item']->value['resize_mode']) {?> selected<?php }?> data-annotation="<?php echo iaSmarty::lang(array('key'=>'fit_tip'),$_smarty_tpl);?>
"><?php echo iaSmarty::lang(array('key'=>'fit'),$_smarty_tpl);?>
</option>
							</select>
							<p class="help-block"></p>
						</div>
					</div>
				<?php }?>
			</div>

			<div id="tree" class="field_type" style="display: none;">
				<a href="#tree"></a>
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'nodes'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<button class="js-tree-action btn btn-xs btn-success" data-action="create"><i class="i-plus"></i> Add Node</button>
						<button class="js-tree-action btn btn-xs btn-danger disabled" data-action="delete"><i class="i-minus"></i> Delete</button>
						<button class="js-tree-action btn btn-xs btn-info disabled" data-action="update"><i class="i-edit"></i> Rename</button>
						<span class="help-inline pull-right"><?php echo iaSmarty::lang(array('key'=>'drag_to_reorder'),$_smarty_tpl);?>
</span>

						<input type="hidden" name="nodes"<?php if (iaField::TREE==$_smarty_tpl->tpl_vars['item']->value['type']) {?> value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['values'], ENT_QUOTES, 'UTF-8', true);?>
"<?php }?>>
						<div class="categories-tree" id="input-nodes"></div>
					</div>
				</div>
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'multiple_selection'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['multiple_selection'];?>
"><i class="i-info"></i></a></label>

					<div class="col col-lg-4">
						<?php echo smarty_function_html_radio_switcher(array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['timepicker'])===null||$tmp==='' ? 0 : $tmp),'name'=>'multiple'),$_smarty_tpl);?>

					</div>
				</div>
			</div>

			<div id="js-multiple" class="field_type" style="display: none;">
				<div id="show_in_search_as" class="row"<?php if (!$_smarty_tpl->tpl_vars['item']->value['searchable']) {?> style="display:none"<?php }?>>
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'show_in_search_as'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<select name="show_as" id="showAs">
							<option value="checkbox"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['show_as'])&&iaField::CHECKBOX==$_smarty_tpl->tpl_vars['item']->value['show_as']) {?> selected<?php }?>><?php echo iaSmarty::lang(array('key'=>'checkboxes'),$_smarty_tpl);?>
</option>
							<option value="radio"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['show_as'])&&iaField::RADIO==$_smarty_tpl->tpl_vars['item']->value['show_as']) {?> selected<?php }?>><?php echo iaSmarty::lang(array('key'=>'radios'),$_smarty_tpl);?>
</option>
							<option value="combo"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['show_as'])&&iaField::COMBO==$_smarty_tpl->tpl_vars['item']->value['show_as']) {?> selected<?php }?>><?php echo iaSmarty::lang(array('key'=>'dropdown'),$_smarty_tpl);?>
</option>
						</select>
					</div>
				</div>

				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_default'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<input type="text" readonly name="multiple_default" id="multiple_default" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['default'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['default'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
						<a href="#" class="js-actions label label-default pull-right" data-action="clearDefault"><i class="i-cancel-circle"></i> <?php echo iaSmarty::lang(array('key'=>'clear_default'),$_smarty_tpl);?>
</a>
					</div>
				</div>

				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'field_values'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<?php if (isset($_smarty_tpl->tpl_vars['item']->value['values'])&&$_smarty_tpl->tpl_vars['item']->value['values']) {?>
							<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['item']->value['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
								<div id="item-value-<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" class="wrap-row wrap-block" data-value-id="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
">
									<div class="row">
										<label class="col col-lg-4 control-label"><?php echo iaSmarty::lang(array('key'=>'key'),$_smarty_tpl);?>
 <i>(<?php echo iaSmarty::lang(array('key'=>'not_required'),$_smarty_tpl);?>
)</i></label>
										<div class="col col-lg-8">
											<input type="text" name="keys[]" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['keys'][$_smarty_tpl->tpl_vars['key']->value])) {?><?php echo $_smarty_tpl->tpl_vars['item']->value['keys'][$_smarty_tpl->tpl_vars['key']->value];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php }?>">
										</div>
									</div>
									<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
 $_smarty_tpl->tpl_vars['code']->value = $_smarty_tpl->tpl_vars['language']->key;
?>
										<div class="row">
											<label class="col col-lg-4 control-label"><?php echo iaSmarty::lang(array('key'=>'item_value'),$_smarty_tpl);?>
 <span class="label label-info"><?php echo $_smarty_tpl->tpl_vars['language']->value['title'];?>
</span></label>
											<div class="col col-lg-8">
												<?php if (@constant('IA_LANGUAGE')==$_smarty_tpl->tpl_vars['code']->value) {?>
													<input type="text" class="fvalue" name="values[]" value="<?php if (!isset($_smarty_tpl->tpl_vars['item']->value['values_titles'][$_smarty_tpl->tpl_vars['value']->value][$_smarty_tpl->tpl_vars['code']->value])) {?><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['item']->value['values_titles'][$_smarty_tpl->tpl_vars['value']->value][$_smarty_tpl->tpl_vars['code']->value];?>
<?php }?>">
												<?php } else { ?>
													<input type="text" name="lang_values[<?php echo $_smarty_tpl->tpl_vars['code']->value;?>
][]" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['lang_values'][$_smarty_tpl->tpl_vars['code']->value][$_smarty_tpl->tpl_vars['key']->value])) {?><?php echo $_smarty_tpl->tpl_vars['item']->value['lang_values'][$_smarty_tpl->tpl_vars['code']->value][$_smarty_tpl->tpl_vars['key']->value];?>
<?php } elseif (isset($_smarty_tpl->tpl_vars['item']->value['values_titles'][$_smarty_tpl->tpl_vars['value']->value][$_smarty_tpl->tpl_vars['code']->value])) {?><?php echo $_smarty_tpl->tpl_vars['item']->value['values_titles'][$_smarty_tpl->tpl_vars['value']->value][$_smarty_tpl->tpl_vars['code']->value];?>
<?php }?>">
												<?php }?>
											</div>
										</div>
									<?php } ?>
									<div class="actions-panel">
										<a href="#" class="js-actions label label-default" data-action="setDefault"><?php echo iaSmarty::lang(array('key'=>'set_as_default_value'),$_smarty_tpl);?>
</a>
										<a href="#" class="js-actions label label-danger" data-action="removeItem" title="<?php echo iaSmarty::lang(array('key'=>'remove'),$_smarty_tpl);?>
"><i class="i-close"></i></a>
										<a href="#" class="js-actions label label-success itemUp" style="display: none;" data-action="itemUp" title="<?php echo iaSmarty::lang(array('key'=>'item_up'),$_smarty_tpl);?>
"><i class="i-chevron-up"></i></a>
										<a href="#" class="js-actions label label-success itemDown" style="display: none;" data-action="itemDown" title="<?php echo iaSmarty::lang(array('key'=>'item_down'),$_smarty_tpl);?>
"><i class="i-chevron-down"></i></a>
									</div>
									<div class="main_fields"<?php if ($_smarty_tpl->tpl_vars['item']->value['relation']!=iaField::RELATION_PARENT) {?> style="display:none;"<?php }?>>
										<?php echo iaSmarty::lang(array('key'=>'field_element_children'),$_smarty_tpl);?>
: <span onclick="wfields(this)"><i class="i-fire"></i></span>
										<span class="list"></span>
										<input type="hidden" name="children[]">
									</div>
								</div>
							<?php } ?>
							<a href="#" class="js-actions label pull-right label-success" id="js-cmd-add-value"><i class="i-plus"></i> <?php echo iaSmarty::lang(array('key'=>'add_item_value'),$_smarty_tpl);?>
</a>
						<?php } else { ?>
							<div id="item-value-default" class="wrap-row wrap-block">
								<div class="row">
									<label class="col col-lg-4 control-label"><?php echo iaSmarty::lang(array('key'=>'key'),$_smarty_tpl);?>
 <i>(<?php echo iaSmarty::lang(array('key'=>'not_required'),$_smarty_tpl);?>
)</i></label>
									<div class="col col-lg-8">
										<input type="text" name="keys[]">
									</div>
								</div>
								<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_smarty_tpl->tpl_vars['code'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value) {
$_smarty_tpl->tpl_vars['language']->_loop = true;
 $_smarty_tpl->tpl_vars['code']->value = $_smarty_tpl->tpl_vars['language']->key;
?>
									<div class="row">
										<label class="col col-lg-4 control-label"><?php echo iaSmarty::lang(array('key'=>'item_value'),$_smarty_tpl);?>
 <span class="label label-info"><?php echo $_smarty_tpl->tpl_vars['language']->value['title'];?>
</span></label>
										<div class="col col-lg-8">
											<?php if ($_smarty_tpl->tpl_vars['code']->value==@constant('IA_LANGUAGE')) {?>
												<input type="text" class="fvalue" name="values[]">
											<?php } else { ?>
												<input type="text" name="lang_values[<?php echo $_smarty_tpl->tpl_vars['code']->value;?>
][]">
											<?php }?>
										</div>
									</div>
								<?php } ?>
								<div class="actions-panel">
									<a href="#" class="js-actions label label-default" data-action="setDefault"><?php echo iaSmarty::lang(array('key'=>'set_as_default_value'),$_smarty_tpl);?>
</a>
									<a href="#" class="js-actions label label-danger" data-action="removeItem" title="<?php echo iaSmarty::lang(array('key'=>'remove'),$_smarty_tpl);?>
"><i class="i-close"></i></a>
									<a href="#" class="js-actions label label-success itemUp" style="display: none;" data-action="itemUp" title="<?php echo iaSmarty::lang(array('key'=>'item_up'),$_smarty_tpl);?>
"><i class="i-chevron-up"></i></a>
									<a href="#" class="js-actions label label-success itemDown" style="display: none;" data-action="itemDown" title="<?php echo iaSmarty::lang(array('key'=>'item_down'),$_smarty_tpl);?>
"><i class="i-chevron-down"></i></a>
								</div>
								<div class="main_fields"<?php if ($_smarty_tpl->tpl_vars['item']->value['relation']!=iaField::RELATION_PARENT) {?> style="display:none;"<?php }?>>
									<?php echo iaSmarty::lang(array('key'=>'field_element_children'),$_smarty_tpl);?>
: <span onclick="wfields(this)"><i class="i-fire"></i></span>
									<?php if (isset($_smarty_tpl->tpl_vars['item']->value['children'][$_smarty_tpl->getVariable('smarty',null,true,false)->value['foreach']['values']['index']])) {?>
										<span class="list"><?php echo $_smarty_tpl->tpl_vars['item']->value['children'][$_smarty_tpl->getVariable('smarty')->value['foreach']['values']['index']]['titles'];?>
</span>
										<input type="hidden" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['children'][$_smarty_tpl->getVariable('smarty')->value['foreach']['values']['index']]['values'];?>
" name="children[]">
									<?php } else { ?>
										<span class="list"></span>
										<input type="hidden" name="children[]">
									<?php }?>
								</div>
							</div>
							<a href="#" class="js-actions label pull-right label-success" id="js-cmd-add-value"><i class="i-plus"></i> <?php echo iaSmarty::lang(array('key'=>'add_item_value'),$_smarty_tpl);?>
</a>
						<?php }?>
					</div>
				</div>
			</div>

			<div id="url" class="field_type" style="display: none;">
				<div class="row">
					<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'url_nofollow'),$_smarty_tpl);?>
</label>

					<div class="col col-lg-4">
						<?php echo smarty_function_html_radio_switcher(array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['url_nofollow'])===null||$tmp==='' ? 0 : $tmp),'name'=>'url_nofollow'),$_smarty_tpl);?>

					</div>
				</div>
			</div>

			<?php if (iaCore::ACTION_EDIT!=$_smarty_tpl->tpl_vars['pageAction']->value) {?>
				<div id="date" class="field_type" style="display: none;">
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'timepicker'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<?php echo smarty_function_html_radio_switcher(array('value'=>(($tmp = @$_smarty_tpl->tpl_vars['item']->value['timepicker'])===null||$tmp==='' ? 0 : $tmp),'name'=>'timepicker'),$_smarty_tpl);?>

						</div>
					</div>
				</div>
			<?php } else { ?>
				<input type="hidden" name="timepicker" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['timepicker'];?>
">
			<?php }?>

			<div id="pictures" class="field_type" style="display: none;">
				<?php if (!is_writeable(@constant('IA_UPLOADS'))) {?>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'pictures'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<div class="alert alert-warning"><?php echo iaSmarty::lang(array('key'=>'upload_writable_permission'),$_smarty_tpl);?>
</div>
						</div>
					</div>
				<?php } else { ?>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'max_num_images'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<input type="text" name="pic_max_images" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['pic_max_images'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['pic_max_images'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>5<?php }?>">
						</div>
					</div>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'file_prefix'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<input type="text" name="pic_file_prefix" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['file_prefix'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['file_prefix'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
						</div>
					</div>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'image_width'),$_smarty_tpl);?>
 / <?php echo iaSmarty::lang(array('key'=>'image_height'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<div class="row">
								<div class="col col-lg-6">
									<input type="text" name="pic_image_width" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['image_width'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['image_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>900<?php }?>">
								</div>
								<div class="col col-lg-6">
									<input type="text" name="pic_image_height" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['image_height'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['image_height'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?>600<?php }?>">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'thumb_width'),$_smarty_tpl);?>
 / <?php echo iaSmarty::lang(array('key'=>'thumb_height'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<div class="row">
								<div class="col col-lg-6">
									<input type="text" name="pic_thumb_width" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['thumb_width'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['thumb_width'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['core']->value['config']['thumb_w'];?>
<?php }?>">
								</div>
								<div class="col col-lg-6">
									<input type="text" name="pic_thumb_height" value="<?php if (isset($_smarty_tpl->tpl_vars['item']->value['thumb_height'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['thumb_height'], ENT_QUOTES, 'UTF-8', true);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['core']->value['config']['thumb_h'];?>
<?php }?>">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'resize_mode'),$_smarty_tpl);?>
</label>

						<div class="col col-lg-4">
							<select name="pic_resize_mode">
								<option value="crop"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['pic_resize_mode'])&&iaPicture::CROP==$_smarty_tpl->tpl_vars['item']->value['pic_resize_mode']) {?> selected<?php }?> data-annotation="<?php echo iaSmarty::lang(array('key'=>'crop_tip'),$_smarty_tpl);?>
"><?php echo iaSmarty::lang(array('key'=>'crop'),$_smarty_tpl);?>
</option>
								<option value="fit"<?php if (isset($_smarty_tpl->tpl_vars['item']->value['pic_resize_mode'])&&iaPicture::FIT==$_smarty_tpl->tpl_vars['item']->value['pic_resize_mode']) {?> selected<?php }?> data-annotation="<?php echo iaSmarty::lang(array('key'=>'fit_tip'),$_smarty_tpl);?>
"><?php echo iaSmarty::lang(array('key'=>'fit'),$_smarty_tpl);?>
</option>
							</select>
							<p class="help-block"></p>
						</div>
					</div>
				<?php }?>
			</div>

			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'required_field'),$_smarty_tpl);?>
</label>

				<div class="col col-lg-4">
					<?php echo smarty_function_html_radio_switcher(array('value'=>$_smarty_tpl->tpl_vars['item']->value['required'],'name'=>'required'),$_smarty_tpl);?>

				</div>
			</div>

			<div class="row" id="tr_required"<?php if (!$_smarty_tpl->tpl_vars['item']->value['required']) {?> style="display: none;"<?php }?>>
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'required_checks'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['required_checks'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-8">
					<textarea name="required_checks" class="js-code-editor"><?php if (isset($_smarty_tpl->tpl_vars['item']->value['required_checks'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['required_checks'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
				</div>
			</div>

			<div class="row">
				<label class="col col-lg-2 control-label"><?php echo iaSmarty::lang(array('key'=>'extra_actions'),$_smarty_tpl);?>
 <a href="#" class="js-tooltip" title="<?php echo $_smarty_tpl->tpl_vars['tooltips']->value['extra_actions'];?>
"><i class="i-info"></i></a></label>

				<div class="col col-lg-8">
					<textarea name="extra_actions" class="js-code-editor"><?php if (isset($_smarty_tpl->tpl_vars['item']->value['extra_actions'])) {?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['extra_actions'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?></textarea>
				</div>
			</div>
		</div>

		<?php echo $_smarty_tpl->getSubTemplate ('fields-system.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

	</div>
</form>
<?php echo iaSmarty::ia_add_media(array('files'=>'tree'),$_smarty_tpl);?>

<?php echo iaSmarty::ia_print_js(array('files'=>'jquery/plugins/jquery.numeric,utils/edit_area/edit_area,admin/fields'),$_smarty_tpl);?>
<?php }} ?>
