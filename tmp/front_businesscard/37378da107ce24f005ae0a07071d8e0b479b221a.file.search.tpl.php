<?php /* Smarty version Smarty-3.1.19, created on 2016-05-17 04:55:11
         compiled from "/Users/dmitry/Desktop/авангард-клининг.рф/templates/common/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:37203983573adc6f566af9-02508069%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '37378da107ce24f005ae0a07071d8e0b479b221a' => 
    array (
      0 => '/Users/dmitry/Desktop/авангард-клининг.рф/templates/common/search.tpl',
      1 => 1455472436,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '37203983573adc6f566af9-02508069',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'query' => 0,
    'regular' => 0,
    'itemName' => 0,
    'results' => 0,
    'data' => 0,
    'item' => 0,
    'pagination' => 0,
    'empty' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_573adc6f6eebf6_18565219',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_573adc6f6eebf6_18565219')) {function content_573adc6f6eebf6_18565219($_smarty_tpl) {?><?php if (!is_callable('smarty_function_ia_hooker')) include '/Users/dmitry/Desktop/авангард-клининг.рф/includes/smarty/intelli_plugins/function.ia_hooker.php';
?><?php if ($_smarty_tpl->tpl_vars['query']->value||$_smarty_tpl->tpl_vars['regular']->value) {?>
	<form class="ia-form">
		<div class="input-group">
			<input type="text" class="form-control" name="q" id="input-search-query" placeholder="<?php echo iaSmarty::lang(array('key'=>'search_for'),$_smarty_tpl);?>
" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['query']->value, ENT_QUOTES, 'UTF-8', true);?>
">
			<span class="input-group-btn">
				<button class="btn btn-primary" type="submit"><?php echo iaSmarty::lang(array('key'=>'search'),$_smarty_tpl);?>
</button>
			</span>
		</div>
	</form>
<?php } else { ?>
	<div class="js-search-sorting-header">
		<?php ob_start();?><?php echo ucfirst($_smarty_tpl->tpl_vars['itemName']->value);?>
<?php $_tmp1=ob_get_clean();?><?php echo smarty_function_ia_hooker(array('name'=>"smartyFrontSearchSorting".$_tmp1),$_smarty_tpl);?>

	</div>
<?php }?>

<div id="js-search-results-container">
	<?php if ($_smarty_tpl->tpl_vars['results']->value) {?>
		<?php if ($_smarty_tpl->tpl_vars['regular']->value) {?>
			<?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['data']->_loop = false;
 $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['results']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
 $_smarty_tpl->tpl_vars['item']->value = $_smarty_tpl->tpl_vars['data']->key;
?>
				<?php if ($_smarty_tpl->tpl_vars['data']->value[0]) {?>
					<div class="search-results">
						<h3 class="title"><?php echo iaSmarty::lang(array('key'=>$_smarty_tpl->tpl_vars['item']->value),$_smarty_tpl);?>
</h3>
						<?php echo $_smarty_tpl->tpl_vars['data']->value[1];?>

					</div>
					<?php if ($_smarty_tpl->tpl_vars['data']->value[0]>$_smarty_tpl->tpl_vars['pagination']->value['limit']&&'pages'!=$_smarty_tpl->tpl_vars['item']->value) {?>
						<div class="text-center m-t search-results-more">
							…<?php echo iaSmarty::lang(array('key'=>'and_more'),$_smarty_tpl);?>

							<a href="<?php echo @constant('IA_URL');?>
search/<?php echo $_smarty_tpl->tpl_vars['item']->value;?>
/?q=<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
" class="btn btn-success"><?php echo iaSmarty::lang(array('key'=>'show_all_num_results','num'=>$_smarty_tpl->tpl_vars['data']->value[0]),$_smarty_tpl);?>
</a>
						</div>
					<?php }?>
				<?php }?>
			<?php } ?>
		<?php } else { ?>
			<?php echo $_smarty_tpl->tpl_vars['results']->value[1];?>

		<?php }?>
	<?php } elseif (!$_smarty_tpl->tpl_vars['regular']->value) {?>
		<?php if (!$_smarty_tpl->tpl_vars['empty']->value) {?>
			<div class="message alert"><?php echo iaSmarty::lang(array('key'=>'nothing_found'),$_smarty_tpl);?>
</div>
		<?php }?>
	<?php }?>
</div>

<div id="js-search-results-pagination">
	<?php echo iaSmarty::pagination(array('aTotal'=>$_smarty_tpl->tpl_vars['pagination']->value['total'],'aTemplate'=>$_smarty_tpl->tpl_vars['pagination']->value['url'],'aItemsPerPage'=>$_smarty_tpl->tpl_vars['pagination']->value['limit'],'aNumPageItems'=>5,'aIgnore'=>true),$_smarty_tpl);?>

</div>

<?php echo iaSmarty::ia_print_js(array('files'=>'frontend/search'),$_smarty_tpl);?>
<?php }} ?>
