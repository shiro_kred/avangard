<?php /* Smarty version Smarty-3.1.19, created on 2016-05-17 06:42:43
         compiled from "/Users/dmitry/Desktop/авангард-клининг.рф/templates/businesscard/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18216332265739fdcb5e0ec4-22803533%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '32c9aa338f455802b28e7e192ac7bd659c2e20ea' => 
    array (
      0 => '/Users/dmitry/Desktop/авангард-клининг.рф/templates/businesscard/layout.tpl',
      1 => 1463481759,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18216332265739fdcb5e0ec4-22803533',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5739fdcba38e90_89527976',
  'variables' => 
  array (
    'core' => 0,
    'key' => 0,
    'value' => 0,
    'img' => 0,
    'iaBlocks' => 0,
    'name' => 0,
    'item' => 0,
    'action' => 0,
    '_content_' => 0,
    'manageMode' => 0,
    'previewMode' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5739fdcba38e90_89527976')) {function content_5739fdcba38e90_89527976($_smarty_tpl) {?><?php if (!is_callable('smarty_function_ia_hooker')) include '/Users/dmitry/Desktop/авангард-клининг.рф/includes/smarty/intelli_plugins/function.ia_hooker.php';
if (!is_callable('smarty_modifier_date_format')) include '/Users/dmitry/Desktop/авангард-клининг.рф/includes/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_function_randnum')) include '/Users/dmitry/Desktop/авангард-клининг.рф/includes/smarty/intelli_plugins/function.randnum.php';
?><!DOCTYPE html>
<html lang="<?php echo $_smarty_tpl->tpl_vars['core']->value['language']['iso'];?>
" dir="<?php echo $_smarty_tpl->tpl_vars['core']->value['language']['direction'];?>
">
	<head>
		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeHeadSection'),$_smarty_tpl);?>


		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title><?php echo iaSmarty::ia_print_title(array(),$_smarty_tpl);?>
</title>
		<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['meta-description'];?>
">
		<meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['meta-keywords'];?>
">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="generator" content="Subrion CMS <?php echo $_smarty_tpl->tpl_vars['core']->value['config']['version'];?>
 - Open Source Content Management System">
		<meta name="robots" content="index">
		<meta name="robots" content="follow">
		<meta name="revisit-after" content="1 day">
		<base href="<?php echo @constant('IA_URL');?>
">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
        <link rel="stylesheet" href="templates/businesscard/css/shiroorg.css"/>
		<link rel="shortcut icon" href="<?php if (!empty($_smarty_tpl->tpl_vars['core']->value['config']['site_favicon'])) {?><?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
uploads/<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site_favicon'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
favicon.ico<?php }?>">

		<?php echo iaSmarty::ia_add_media(array('files'=>'jquery, subrion, bootstrap','order'=>0),$_smarty_tpl);?>

		<?php echo iaSmarty::ia_print_js(array('files'=>'_IA_TPL_parallax.min, _IA_TPL_wow.min, _IA_TPL_app','order'=>999),$_smarty_tpl);?>


		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontAfterHeadSection'),$_smarty_tpl);?>


		<?php echo iaSmarty::ia_print_css(array('display'=>'on'),$_smarty_tpl);?>


		<?php $_smarty_tpl->smarty->_tag_stack[] = array('ia_add_js', array()); $_block_repeat=true; echo iaSmarty::ia_add_js(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

			intelli.pageName = '<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['name'];?>
';

			<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['customConfig']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
				intelli.config.<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 = '<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
';
			<?php } ?>
		<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo iaSmarty::ia_add_js(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	</head>

	<body class="page-<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['name'];?>
">
		<header class="header" data-parallax="scroll" data-image-src="<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_bg']) {?><?php echo @constant('IA_URL');?>
uploads/<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_bg'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['img']->value;?>
bg7.jpg<?php }?>">
			<div class="inventory">
				<div class="container">
					<div class="inventory__info">
						<span><span class="fa fa-map-pin"></span> <?php echo iaSmarty::lang(array('key'=>'website_address'),$_smarty_tpl);?>
</span>
						<span><span class="fa fa-phone"></span> <?php echo iaSmarty::lang(array('key'=>'website_phone'),$_smarty_tpl);?>
</span>
					</div>
					<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_header']) {?>
						<div class="social">
							<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_t']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_t'];?>
" class="twitter"><span class="fa fa-twitter"></span></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_f']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_f'];?>
" class="facebook"><span class="fa fa-facebook"></span></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_g']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_g'];?>
" class="google-plus"><span class="fa fa-google-plus"></span></a><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_i']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_i'];?>
" class="linkedin"><span class="fa fa-linkedin"></span></a><?php }?>
						</div>
					<?php }?>
					<?php echo iaSmarty::ia_blocks(array('block'=>'inventory'),$_smarty_tpl);?>

				</div>
			</div>
			<nav class="navbar navbar-default">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand<?php if (!$_smarty_tpl->tpl_vars['core']->value['config']['enable_text_logo']) {?> navbar-brand--img<?php }?>" href="<?php echo @constant('IA_URL');?>
">
							<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['enable_text_logo']) {?>
								<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['logo_text'];?>

							<?php } else { ?>
								<?php if (!empty($_smarty_tpl->tpl_vars['core']->value['config']['site_logo'])) {?>
									<img src="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
uploads/<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site_logo'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site'];?>
">
								<?php } else { ?>
									<img src="<?php echo $_smarty_tpl->tpl_vars['img']->value;?>
logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site'];?>
">
								<?php }?>
							<?php }?>
						</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<?php echo $_smarty_tpl->getSubTemplate ('language-selector.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						<?php echo iaSmarty::ia_blocks(array('block'=>'account'),$_smarty_tpl);?>

						<?php echo iaSmarty::ia_blocks(array('block'=>'mainmenu'),$_smarty_tpl);?>

					</div>
				</div>
			</nav>

			<?php echo iaSmarty::ia_blocks(array('block'=>'teaser'),$_smarty_tpl);?>

		</header>

		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeBreadcrumb'),$_smarty_tpl);?>


		<?php echo $_smarty_tpl->getSubTemplate ('breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


		<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['enable_landing']&&'index'==$_smarty_tpl->tpl_vars['core']->value['page']['name']) {?>
			<div class="landing">
				<?php echo iaSmarty::ia_blocks(array('block'=>'landing'),$_smarty_tpl);?>

			</div>
		<?php } else { ?>
			<?php if (isset($_smarty_tpl->tpl_vars['iaBlocks']->value['verytop'])) {?>
				<div class="verytop">
					<div class="container"><?php echo iaSmarty::ia_blocks(array('block'=>'verytop'),$_smarty_tpl);?>
</div>
				</div>
			<?php }?>

			<div class="content">
				<div class="container">
					<div class="row">
						<div class="<?php echo iaSmarty::width(array('section'=>'content','position'=>'left','tag'=>'col-md-'),$_smarty_tpl);?>
 aside">
							<?php echo iaSmarty::ia_blocks(array('block'=>'left'),$_smarty_tpl);?>

						</div>
						<div class="<?php echo iaSmarty::width(array('section'=>'content','position'=>'center','tag'=>'col-md-'),$_smarty_tpl);?>
">
							<div class="content__wrap">
								<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeNotifications'),$_smarty_tpl);?>

								<?php echo $_smarty_tpl->getSubTemplate ('notification.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


								<?php echo iaSmarty::ia_blocks(array('block'=>'top'),$_smarty_tpl);?>


								<div class="content__header">
									<h1><?php echo $_smarty_tpl->tpl_vars['core']->value['page']['title'];?>
</h1>
									<ul class="content__actions">
										<?php  $_smarty_tpl->tpl_vars['action'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['action']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['actions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['action']->key => $_smarty_tpl->tpl_vars['action']->value) {
$_smarty_tpl->tpl_vars['action']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['action']->key;
?>
											<li>
												<?php if ('action-favorites'==$_smarty_tpl->tpl_vars['name']->value) {?>
													<?php echo iaSmarty::printFavorites(array('item'=>$_smarty_tpl->tpl_vars['item']->value,'itemtype'=>$_smarty_tpl->tpl_vars['item']->value['item']),$_smarty_tpl);?>

												<?php } else { ?>
													<a data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['action']->value['title'];?>
" <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['action']->value['attributes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" <?php } ?>>
														<span class="fa fa-<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
"></span>
													</a>
												<?php }?>
											</li>
										<?php } ?>
									</ul>
								</div>

								<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeMainContent'),$_smarty_tpl);?>


								<div class="content__body">
									<?php echo $_smarty_tpl->tpl_vars['_content_']->value;?>

								</div>

								<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontAfterMainContent'),$_smarty_tpl);?>


								<?php echo iaSmarty::ia_blocks(array('block'=>'bottom'),$_smarty_tpl);?>

							</div>
						</div>
						<div class="<?php echo iaSmarty::width(array('section'=>'content','position'=>'right','tag'=>'col-md-'),$_smarty_tpl);?>
 aside">
							<?php echo iaSmarty::ia_blocks(array('block'=>'right'),$_smarty_tpl);?>

						</div>
					</div>
				</div>
			</div>

			<?php if (isset($_smarty_tpl->tpl_vars['iaBlocks']->value['verybottom'])) {?>
				<div class="verybottom">
					<div class="container"><?php echo iaSmarty::ia_blocks(array('block'=>'verybottom'),$_smarty_tpl);?>
</div>
				</div>
			<?php }?>
		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['iaBlocks']->value['footer1'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['footer2'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['footer3'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['footer4'])) {?>
			<div class="footer-blocks">
				<div class="container">
					<div class="row">
						<div class="<?php echo iaSmarty::width(array('section'=>'footer-blocks','position'=>'footer1','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'footer1'),$_smarty_tpl);?>
</div>
						<div class="<?php echo iaSmarty::width(array('section'=>'footer-blocks','position'=>'footer2','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'footer2'),$_smarty_tpl);?>
</div>
						<div class="<?php echo iaSmarty::width(array('section'=>'footer-blocks','position'=>'footer3','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'footer3'),$_smarty_tpl);?>
</div>
						<div class="<?php echo iaSmarty::width(array('section'=>'footer-blocks','position'=>'footer4','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'footer4'),$_smarty_tpl);?>
</div>
					</div>
				</div>
			</div>
		<?php }?>

		<footer class="footer">
            <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter37402455 = new Ya.Metrika({ id:37402455, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <!-- /Yandex.Metrika counter -->
			<div class="container">
				<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeFooterLinks'),$_smarty_tpl);?>


				<p class="copyright">&copy; <?php echo smarty_modifier_date_format($_SERVER['REQUEST_TIME'],'%Y');?>
 <?php echo iaSmarty::lang(array('key'=>'powered_by_subrion'),$_smarty_tpl);?>
</p>

				<a href="#" class="back-to-top js-back-to-top" title="<?php echo iaSmarty::lang(array('key'=>'back_to_top'),$_smarty_tpl);?>
"><span class="fa fa-angle-up"></span></a>

				<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_footer']) {?>
					<div class="social">
						<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_t']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_t'];?>
" class="twitter"><span class="fa fa-twitter"></span></a><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_f']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_f'];?>
" class="facebook"><span class="fa fa-facebook"></span></a><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_g']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_g'];?>
" class="google-plus"><span class="fa fa-google-plus"></span></a><?php }?>
						<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_i']) {?><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_i'];?>
" class="linkedin"><span class="fa fa-linkedin"></span></a><?php }?>
					</div>
				<?php }?>

				<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontAfterFooterLinks'),$_smarty_tpl);?>

			</div>
		</footer>

		<!-- SYSTEM STUFF -->

		<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['cron']) {?>
			<div style="display: none;">
				<img src="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
cron/?<?php echo smarty_function_randnum(array(),$_smarty_tpl);?>
" width="1" height="1" alt="">
			</div>
		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['manageMode']->value)) {?>
			<?php echo $_smarty_tpl->getSubTemplate ('visual-mode.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['previewMode']->value)) {?>
			<p><?php echo iaSmarty::lang(array('key'=>'youre_in_preview_mode'),$_smarty_tpl);?>
</p>
		<?php }?>

		<?php echo iaSmarty::ia_print_js(array('display'=>'on'),$_smarty_tpl);?>


		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontFinalize'),$_smarty_tpl);?>

	</body>
</html><?php }} ?>
